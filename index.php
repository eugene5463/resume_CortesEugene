<!DOCTYPE html>
<html>
    <head>
        <title>YUJIN</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="text/javascript" type="text/javascript" href="js/scripts.js">
        <link rel="text/javascript" type="text/javascript" href="js/jquery.js">
        
    </head>
    
    <body background="images/bg6.jpeg" style="background-attachment: fixed; background-position: center" >
        
    
        <div class="header-container" >
            
            <nav>
		<ul>
                    <li><a onclick="click()"  href="#index">HOME</a></li>
                    <li><a onclick="click()"  href="#about">ABOUT</a></li>
                    <li><a onclick="click()"  href="#portfolio">PORTFOLIO</a></li>
                    <li><a onclick="click()"  href="#contact">CONTACT</a></li>
                    <li><a onclick="click()"  href="#resume">RESUME</a></li>
		</ul>
            </nav>
        </div>
        <!-- ayaw ni sya hilabti nga id sa home na -->
        <div id="index" style="color: transparent"> 
         `
        </div>
        
        <article class="title-container">
            <span class="title"> 
                < / Y U J I N >
            </span>
            <span class="subtitle">
                UI | UX | FRONT-END | BACK-END
            </span>
        </article>
         
        <div class="gap"></div><div class="gap"></div>
        <div class="gap"></div><div class="gap"></div>
        <div class="gap"></div><div class="gap"></div> <!-- pasensyahi ang tulo-->
            
        </div>
        <article class="pane-container"  id="about">
            <center>
                <h1 style="font-size: 4vw;"> ABOUT </h1>
                <hr style="width: 90%">
                
                <img class="about-image" src="images/me.png" alt="">
                
                
                <div class="right-side">
                    
                    
                        Hi I'm Eugene Cortes
                        <br>
                        I am learning to be a full stack developer
                        currently streamlining web dev
                        <br>
                        and also a mobile tech enthusiast
                </div>
            
                
                
            </center>     
        </article>
        
        <div class="gap"></div>
  
        
        <article class="portfolio-container" id="portfolio">
            <center>
                
           
        
            <!-- language container-->
            <div class="language-container">
                
                <center>
                    LANGUAGES
                    <hr>           
                <p>        
                    JAVA
                    <br>
                    C++
                    <br>
                    HTML  CSS
                    <br>
                    JAVASCRIPT
                    <br>
                    C#
                    <br>
                    SQL
                    <br>
                    Python
                </p>       
                </center>
            </div>

            
             <!-- experienced container-->
             <div class="software-container">
            
            <center>
                    EXPERIENCED IN
                    <hr>           
                    <p>        
                    ADOBE PHOTOSHOP
                    <br>
                    ADOBE ILLUSTRATOR
                    <br>
                    ADOBE MACROMEDIA
                    <br>
                    MICROSOFT OFFICE
                    <br>
                    UNITY ENGINE
                    <br>
                    OUTSYSTEMS
                    <br>
                    MARIA DB
                    <br>
                </p>       
                </center>
            
            
            </div>
              </center> 
        </article>
        
        <div class="gap"></div>
        <div class="gap"></div>
        
        <article>
            
            <div class="skill-container">
                <center>
                    
                    SKILLS
                    <hr><br>
                    Proficient in Office applications<br>
                    Excellent communication skill<br>
                    Decision Making<br>
                    Critical thinking<br>
                    Copywriting<br>

                    
                </center>
            </div>
            
            <div class="gap">
                
            </div>
        
        </article>
        <div class="gap"  id="contact"></div>
         <!-- contact area-->
        <article class="contact-container" >
                 
            
                <p class="contact-tag">FOLLOW ME AT</p>
                <div class="contact-icon">
                        <a href="https://www.facebook.com/hyujinshi"><img class="contact-icon"  src="images/facebook.png" alt="" style="width: 60px; height: 60px;"  > </a>
                        <a href="https://www.instagram.com/madhyujin"><img class="contact-icon"  src="images/instagram.png" alt="" style="width: 60px; height: 60px;"  ></a> 
                        <a href="https://www.twitter.com/madhyujin"><img class="contact-icon"  src="images/twitter.png" alt="" style="width: 60px; height: 60px;"  ></a>
                        <a href="https://www.linkedin.com/in/eugene-cortes-2682b8162/">  <img class="contact-icon"  src="images/linkedin.png" alt="" style="width: 60px; height: 60px;"  ></a>
                </div>
            
            
        </article>
        
         <div class="gap"></div><div class="gap"></div>
         <div class="gap" id="resume"></div>
         
         <!-- Lote sa resume -->
         <article class="resume-container">
             <div class="resume-image">
                 <img  class="" src="images/me.png" alt="">
             </div>
            
             <div class="resume-content">
                  <hr>
                  <h1>Personal</h1>
                 Eugene Lucagbo Cortes <br>
                 20 years old<br> Panacan Davao City, Philippines <br>
                 Single
             <hr>
            
                 
                <h1>Education</h1>
                 University of Southeastern Philippines<br>
                 BAchelor of Science in Informatio Technology<br>
                 3rd year<br>
                 Dr. Santiago Sr. Nat'l High School (DSDSNHS)<br>
                 Graduated as a Valedictorian
             
              <hr>
             
                  <h1>Achievements</h1>
                  Student Council Local Secretary (2018-present)<br>
                  1st Place - Innofest, Information Management<br>
                  Recipient of Vice President Excellence award (Supreme student Government, DSDSNHS) (2016)<br>
                  
                  
                  
               <hr>   
               <h1>Work Experience</h1>
                  Freelance Music Tutor, Panacan Early Childhood Development Center (PECDC)<br>
                  Taught Violin and Piano 2014-2017

               
               <hr>
             
                   <h1>References</h1> 
                  Dr. Hazel Licong Taladua<br>
                  Head, Summer Music Tutorial (PECDC)<br>
                  +63 928 267 6891<br>
                  +63 922 607 3472<br>

              </div>
             
         </article>
          <div class="gap">
    <script src="js/jquery-2.1.3.min.js" > </script>
    <script src="js/scripts.js" > </script>
    
    </body>
</html>
